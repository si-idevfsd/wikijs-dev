# Getting Started

1. `make`
1. Set up your Wiki at http://localhost:3001/
    - Site URL should be http://localhost:3001 too

## Enable Tequila Strategy

1. [Go to your site's authentication settings](http://localhost:3001/a/auth) (can also be reached via ⚙ (top right) → Authentication (left menu))
1. Click the blue Add Strategy button
1. Scroll down to Tequila (second to last)
1. Click the green Apply button (top right)

Control:

1. Log out from the account icon (top right)
1. Browse http://localhost:3001/login
1. Under Select Authentication Provider, you should see Tequila

## Run with Debugger

1. Uncomment the relevant lines in `docker-compose.yml`
1. Run the whole thing again. Suggestion is to use a split-window setup with `docker-compose up -d db` in one window, and `docker-compose up wiki` in another
